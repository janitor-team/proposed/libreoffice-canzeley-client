-- MySQL dump 10.13  Distrib 5.5.58, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: Canzeley
-- ------------------------------------------------------
-- Server version	5.5.58-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `Canzeley`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `Canzeley` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `Canzeley`;

--
-- Table structure for table `Adressen`
--

DROP TABLE IF EXISTS `Adressen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Adressen` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PersonengruppenNr` int(11) NOT NULL DEFAULT '1',
  `Firmennamen` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Anrede` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `BriefAnrede` varchar(100) COLLATE latin1_german1_ci DEFAULT NULL,
  `Vorname` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Nachname` varchar(50) COLLATE latin1_german1_ci NOT NULL DEFAULT '',
  `EhepartnerName` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Strasse` varchar(70) COLLATE latin1_german1_ci DEFAULT NULL,
  `Ort` varchar(70) COLLATE latin1_german1_ci DEFAULT NULL,
  `PLZ` varchar(20) COLLATE latin1_german1_ci DEFAULT NULL,
  `EMailAdresse` varchar(70) COLLATE latin1_german1_ci DEFAULT NULL,
  `TelefonPrivat` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `TelefonBeruflich` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `DurchwahlBuero` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `MobilesTelefon` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `Faxnummer` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `Geburtsdatum` date DEFAULT NULL,
  `Anmerkungen` varchar(255) COLLATE latin1_german1_ci DEFAULT NULL,
  `Kontonummer` varchar(15) COLLATE latin1_german1_ci DEFAULT NULL,
  `BLZ` int(10) DEFAULT NULL,
  `Bankname` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `Homepage` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Anlagedatum` date NOT NULL,
  `Aenderungsdatum` date NOT NULL,
  `Name2` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Land` varchar(40) COLLATE latin1_german1_ci DEFAULT NULL,
  `Postfach` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `Zeichen` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `Umsatzsteuerberechnung` varchar(5) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Adressen`
--

LOCK TABLES `Adressen` WRITE;
/*!40000 ALTER TABLE `Adressen` DISABLE KEYS */;
/*!40000 ALTER TABLE `Adressen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Akten`
--

DROP TABLE IF EXISTS `Akten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Akten` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RegisterNr` varchar(15) COLLATE latin1_german1_ci NOT NULL,
  `Anlagedatum` date NOT NULL,
  `ReferatNr` int(11) DEFAULT NULL,
  `Sachbearbeiter` int(11) DEFAULT NULL,
  `Kurzrubrum` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  `Wegen` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Aktenzeichen` varchar(7) COLLATE latin1_german1_ci DEFAULT NULL,
  `Anmerkung` varchar(255) COLLATE latin1_german1_ci DEFAULT NULL,
  `LetzteAenderung` date NOT NULL,
  `Akteabgelegt` char(3) COLLATE latin1_german1_ci DEFAULT NULL,
  `Ablagedatum` date DEFAULT NULL,
  `AblageNr` varchar(7) COLLATE latin1_german1_ci DEFAULT NULL,
  `AblageNotizen` varchar(255) COLLATE latin1_german1_ci DEFAULT NULL,
  `Herkunft` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Akten`
--

LOCK TABLES `Akten` WRITE;
/*!40000 ALTER TABLE `Akten` DISABLE KEYS */;
/*!40000 ALTER TABLE `Akten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AktenAdressen`
--

DROP TABLE IF EXISTS `AktenAdressen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AktenAdressen` (
  `ID` int(9) NOT NULL AUTO_INCREMENT,
  `AktenNr` int(9) NOT NULL,
  `AdressenNr` int(9) NOT NULL,
  `PersonengruppenNr` int(10) NOT NULL DEFAULT '1',
  `Zeichen` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Ansprechperson` varchar(55) COLLATE latin1_german1_ci DEFAULT NULL,
  `TelefonAP` varchar(20) COLLATE latin1_german1_ci DEFAULT NULL,
  `TelefaxAP` varchar(20) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AktenAdressen`
--

LOCK TABLES `AktenAdressen` WRITE;
/*!40000 ALTER TABLE `AktenAdressen` DISABLE KEYS */;
/*!40000 ALTER TABLE `AktenAdressen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AktenGerichte`
--

DROP TABLE IF EXISTS `AktenGerichte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AktenGerichte` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AktenNr` int(11) NOT NULL,
  `GerichteNr` int(11) NOT NULL,
  `Aktenzeichen` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Kurzrubrum` varchar(100) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AktenGerichte`
--

LOCK TABLES `AktenGerichte` WRITE;
/*!40000 ALTER TABLE `AktenGerichte` DISABLE KEYS */;
/*!40000 ALTER TABLE `AktenGerichte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnredeBriefanrede`
--

DROP TABLE IF EXISTS `AnredeBriefanrede`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AnredeBriefanrede` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Anrede` varchar(100) COLLATE latin1_german1_ci DEFAULT NULL,
  `Briefanrede` varchar(100) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnredeBriefanrede`
--

LOCK TABLES `AnredeBriefanrede` WRITE;
/*!40000 ALTER TABLE `AnredeBriefanrede` DISABLE KEYS */;
INSERT INTO `AnredeBriefanrede` VALUES (1,'Herr','Sehr geehrter Herr'),(2,'Firma','Sehr geehrte Damen und Herren');
/*!40000 ALTER TABLE `AnredeBriefanrede` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Gerichte`
--

DROP TABLE IF EXISTS `Gerichte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Gerichte` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GerichtsartNr` int(11) NOT NULL,
  `Gericht` varchar(60) COLLATE latin1_german1_ci NOT NULL,
  `OrtNr` int(6) DEFAULT NULL,
  `PLZ` varchar(10) COLLATE latin1_german1_ci DEFAULT NULL,
  `Strasse` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `Telefon` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  `Telefax` varchar(30) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Gerichte`
--

LOCK TABLES `Gerichte` WRITE;
/*!40000 ALTER TABLE `Gerichte` DISABLE KEYS */;
INSERT INTO `Gerichte` VALUES (1,1,'AG Düsseldorf',1,'40227','Werdener Str. 1','0211-8306-0','0211-87565-1160'),(2,2,'LG Düsseldorf',1,'40227','Werdener Str. 1','0211-8306-0','0211-87565-1260');
/*!40000 ALTER TABLE `Gerichte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Gerichtsart`
--

DROP TABLE IF EXISTS `Gerichtsart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Gerichtsart` (
  `GerichtsartNr` int(11) NOT NULL AUTO_INCREMENT,
  `Gerichtsart` varchar(30) COLLATE latin1_german1_ci NOT NULL DEFAULT '',
  `AbKuerzung` varchar(10) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`GerichtsartNr`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Gerichtsart`
--

LOCK TABLES `Gerichtsart` WRITE;
/*!40000 ALTER TABLE `Gerichtsart` DISABLE KEYS */;
INSERT INTO `Gerichtsart` VALUES (1,'Amtsgericht','AG'),(2,'Landgericht','LG');
/*!40000 ALTER TABLE `Gerichtsart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Herkunft`
--

DROP TABLE IF EXISTS `Herkunft`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Herkunft` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Herkunft` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Herkunft`
--

LOCK TABLES `Herkunft` WRITE;
/*!40000 ALTER TABLE `Herkunft` DISABLE KEYS */;
INSERT INTO `Herkunft` VALUES (1,'frühere Tätigkeit');
/*!40000 ALTER TABLE `Herkunft` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `KontenIBAN`
--

DROP TABLE IF EXISTS `KontenIBAN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `KontenIBAN` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Kontoinhaber` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `Bank` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  `IBAN` varchar(34) COLLATE latin1_german1_ci NOT NULL,
  `BIC` varchar(11) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `KontenIBAN`
--

LOCK TABLES `KontenIBAN` WRITE;
/*!40000 ALTER TABLE `KontenIBAN` DISABLE KEYS */;
/*!40000 ALTER TABLE `KontenIBAN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrtNr`
--

DROP TABLE IF EXISTS `OrtNr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrtNr` (
  `OrtNr` bigint(3) NOT NULL AUTO_INCREMENT,
  `Ort` varchar(50) NOT NULL,
  PRIMARY KEY (`OrtNr`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrtNr`
--

LOCK TABLES `OrtNr` WRITE;
/*!40000 ALTER TABLE `OrtNr` DISABLE KEYS */;
INSERT INTO `OrtNr` VALUES (1,'Düsseldorf');
/*!40000 ALTER TABLE `OrtNr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PersonenKonten`
--

DROP TABLE IF EXISTS `PersonenKonten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PersonenKonten` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AdressenID` int(11) NOT NULL,
  `KontenID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PersonenKonten`
--

LOCK TABLES `PersonenKonten` WRITE;
/*!40000 ALTER TABLE `PersonenKonten` DISABLE KEYS */;
/*!40000 ALTER TABLE `PersonenKonten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Personengruppe`
--

DROP TABLE IF EXISTS `Personengruppe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Personengruppe` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Personengruppe` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Personengruppe`
--

LOCK TABLES `Personengruppe` WRITE;
/*!40000 ALTER TABLE `Personengruppe` DISABLE KEYS */;
INSERT INTO `Personengruppe` VALUES (1,'Mandant'),(2,'Gegner');
/*!40000 ALTER TABLE `Personengruppe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Referat`
--

DROP TABLE IF EXISTS `Referat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Referat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Referat` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Referat`
--

LOCK TABLES `Referat` WRITE;
/*!40000 ALTER TABLE `Referat` DISABLE KEYS */;
INSERT INTO `Referat` VALUES (1,'Zivilsache (allgem.)');
/*!40000 ALTER TABLE `Referat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sachbearbeiter`
--

DROP TABLE IF EXISTS `Sachbearbeiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sachbearbeiter` (
  `Sachbearbeiter-Nr` int(4) NOT NULL AUTO_INCREMENT,
  `Sachbearbeiter` varchar(50) COLLATE latin1_german1_ci NOT NULL,
  PRIMARY KEY (`Sachbearbeiter-Nr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sachbearbeiter`
--

LOCK TABLES `Sachbearbeiter` WRITE;
/*!40000 ALTER TABLE `Sachbearbeiter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Sachbearbeiter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Systemeigenschaften`
--

DROP TABLE IF EXISTS `Systemeigenschaften`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Systemeigenschaften` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `EigenschaftName` varchar(50) NOT NULL,
  `EigenschaftWert` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Systemeigenschaften`
--

LOCK TABLES `Systemeigenschaften` WRITE;
/*!40000 ALTER TABLE `Systemeigenschaften` DISABLE KEYS */;
INSERT INTO `Systemeigenschaften` VALUES (1,'Datenbankversion','51');
/*!40000 ALTER TABLE `Systemeigenschaften` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Vorlagen`
--

DROP TABLE IF EXISTS `Vorlagen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vorlagen` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Vorlagenname` varchar(50) NOT NULL,
  `Beschreibung` varchar(200) DEFAULT NULL,
  `Bezeichnung` varchar(20) DEFAULT NULL,
  `Pfad` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Vorlagen`
--

LOCK TABLES `Vorlagen` WRITE;
/*!40000 ALTER TABLE `Vorlagen` DISABLE KEYS */;
/*!40000 ALTER TABLE `Vorlagen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Wiedervorlagen`
--

DROP TABLE IF EXISTS `Wiedervorlagen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Wiedervorlagen` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AktenID` int(11) NOT NULL,
  `Datum` date DEFAULT NULL,
  `WvlGrund` varchar(50) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Wiedervorlagen`
--

LOCK TABLES `Wiedervorlagen` WRITE;
/*!40000 ALTER TABLE `Wiedervorlagen` DISABLE KEYS */;
/*!40000 ALTER TABLE `Wiedervorlagen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `WvlGrund`
--

DROP TABLE IF EXISTS `WvlGrund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WvlGrund` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `WvlGrund` varchar(50) COLLATE latin1_german1_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WvlGrund`
--

LOCK TABLES `WvlGrund` WRITE;
/*!40000 ALTER TABLE `WvlGrund` DISABLE KEYS */;
INSERT INTO `WvlGrund` VALUES (1,'allg. Kontrolle');
/*!40000 ALTER TABLE `WvlGrund` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-17 10:20:27
