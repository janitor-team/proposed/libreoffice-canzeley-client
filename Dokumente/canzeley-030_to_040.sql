-- SQL-Datei zum Updaten der Datenbank Canzeley
-- von Version 030 nach Version 040

USE Canzeley;

-- Versionsnummer erhoehen

UPDATE `Systemeigenschaften` SET `EigenschaftWert` = '40' WHERE `EigenschaftName` = 'Datenbankversion';

-- Tabelle AktenAdressen ergaenzen

ALTER TABLE AktenAdressen
ADD COLUMN TelefonAP varchar(20) collate latin1_german1_ci default NULL
AFTER Ansprechperson,
ADD COLUMN TelefaxAP varchar(20) collate latin1_german1_ci default NULL
AFTER TelefonAP;
